from rastertimeseriesmanager.core import RasterTimeseriesBuilder

# example file: 19840416_LEVEL2_LND05_BOA
class ForceL2BoaBuilder(RasterTimeseriesBuilder):

    names = 'Blue', 'Green', 'Red', 'NIR', 'SWIR1', 'SWIR2'
    wavelengths = 482, 561, 654, 865, 1608, 2200
    noDataValues = -9999, -9999, -9999, -9999, -9999, -9999
    resolution = 30
    extensions = '.tif',

    def isValidRaster(self, root, file):
        '''Return wether ``file`` (i.e. basename without extension) stored in ``root`` should be considered.'''
        return 'BOA' in file

    def isValidBand(self, root, file, band):
        '''Return wether ``band`` of raster ``file`` stored in ``root`` should be considered.'''
        if 'LND' in file:
            return True # use all bands
        elif 'SEN2' in file:
            return band in [1, 2, 3, 8, 9, 10] # sort out bands that do not match with Landsat
        else:
            raise Exception('unexpected sensor type')

    def date(self, root, file, band):
        '''Return ``(year, month, day)`` date tuple for ``band`` of raster ``file`` stored in ``root``.'''
        year = int(file[0:4])
        month = int(file[4:6])
        day = int(file[6:8])
        return year, month, day

    def name(self, root, file, band):
        '''Return name for ``band`` of raster ``file`` stored in ``root``.'''
        if 'LND' in file:
            index = band - 1
        elif 'SEN2' in file:
            index = {1:1, 2:2, 3:3, 8:4, 9:5, 10:6}[band] -1  # map from Sentinel-2 to Landsat bands
        else:
            raise Exception('unexpected sensor type')
        return self.names[index]
