from rastertimeseriesmanager.core import RasterTimeseriesBuilder

# https://doi.pangaea.de/10.1594/PANGAEA.870486
class PANGAEA_870486(RasterTimeseriesBuilder):

    names = 'Diatoms', 'Coccolithophores', 'Cyanobacteria'
    wavelengths = None
    noDataValues = [9969209968386869046778552952102584320] * 3
    resolution = None
    extensions = '.nc',

    def date(self, root, file, band):
        '''Return ``(year, month, day)`` date tuple for ``band`` of raster ``file`` stored in ``root``.'''
        year = int(file[-6:-2])
        month = int(file[-2:])
        day = 1
        return year, month, day

    def name(self, root, file, subdataset, band):
        '''Return name for ``band`` of raster ``file`` stored in ``root``.'''
        index = ['DIA', 'COC', 'CYA'].index(subdataset)
        return self.names[index]
