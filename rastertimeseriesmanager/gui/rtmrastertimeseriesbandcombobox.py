import math
from osgeo import gdal
from qgis.PyQt.QtWidgets import QComboBox
from qgis.core import QgsMapLayer, QgsRasterLayer
from ..core.rtmrastertimeseries import RtmRasterTimeseries

class RtmRasterTimeseriesBandComboBox(QComboBox):

    def setLayer(self, layer):

        # Check layer type.
        if isinstance(layer, QgsRasterLayer):
            pass
        elif isinstance(layer, (type(None), QgsMapLayer)):
            layer = None
        else:
            raise TypeError('wrong type: {}'.format(type(layer)))

        # Init timeseries and update widget.
        self.timeseries = RtmRasterTimeseries(layer=layer)

        self.blockSignals(True)
        # Remove all items.
        for i in range(self.count()):
            self.removeItem(0)
        if self.timeseries.isValid():
            zwidth = math.floor(math.log10(self.timeseries.numberOfBands())) + 1
            ds = gdal.Open(layer.source())
            bandNames = ['Band {}: {}'.format(str(i+1).zfill(zwidth), name) for i, name in enumerate(self.timeseries.bands())]
            self.addItems(bandNames)
        self.blockSignals(False)
        self.setCurrentIndex(0)
